import './App.css';
import React from 'react';
import Skills from "./components/Skills.js";
import list from  "./components/List.js";

import cvImage from "./images/cv.jpg";


const textStyle = {
  textAlign: 'center',
  marginLeft: '20%',
  marginRight: '20%'
};


const containerStyle = {
  overflow: 'hidden',
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto',
  width: '225px',
  height: '250px',
};


const cvImageStyle = {
  margin: '-10px 0px 0px -25px'
};


const ContactInfo = ({data}) => {
  return (
    <div style={{lineHeight: '10px'}}>
      <p>{data["location"]}</p>
      <p>{data["phone"]}</p>
      <p>{data["email"]}</p>
    </div>
  );
}


const RepoList = ({data}) => {
  return (
    <>
      <h2>Git Repositories</h2>
      {
        Object.keys(data["repos"]).map((item, index) => (
          <a key={index} href={data["repos"][item]}>{item}<br></br></a>
        ))
      }
    </>

  );

}



const App = () => {
  document.title = "OnlineCV"

  const data = require('./cv_data.json');

  return (
    <div className="App">
      <header className="desktop-header">

        <p style={textStyle}>This is an interactive CV (((overkill))) &lt;WIP&gt;</p>


        <div style={containerStyle}>
          <img style={cvImageStyle} alt="cv" src={cvImage}/>
        </div>

        <h1>{data["name"]}</h1>

        <ContactInfo data={data}/>


        <h2>About me</h2>
        <p style={textStyle}>{data["about"]}</p>

        <Skills data={data}/>

        {/*TODO: as History component*/}
        <h2>Experience</h2>

        {/*TODO: as History component*/}
        <h2>Education</h2>

        <list.LinkList title="Projects" data={data}/>
        <list.List title="Languages" data={data}/>
        <list.List title="Interests" data={data}/>


        {/*TODO: with icons*/}
        <RepoList data={data}/>

        <h2>Other</h2>
        <p>{data["other"]}</p>


      </header>
    </div>
  )

}

export default App
