import React from 'react'
import LinearProgress from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';


const remap = (value, min1, max1, min2, max2) => {
    return min2 + ((max2 - min2) / (max1 - min1)) * (value - min1);
}


const limitValue = (value) => {

  const min = 0;
  const max = 100;

  let mapped = remap(value, 0, 6, min, max);

  mapped = mapped > max ? max : mapped;
  mapped = mapped < min ? min : mapped;

  return mapped;
}


const labelStyle = {
  textAlign: 'left',
  marginLeft: '20%'
};


const barBoxStyle = {
  marginRight: '20%',
  width: '80%',
  mr: 1
};


const skillListStyle = {
  textAlign: 'center',
  marginLeft: '20%',
  marginRight: '20%'
};


const Skills = ({data}) => {

  // descending order
  const cmp = (first, second) => {

    if (data["skills"][first] < data["skills"][second]) {
      return 1;

    }

    return -1;
  }

  return (
    <div>
      <h2>Skills</h2>
      {
        Object.keys(data["skills"]).sort(cmp).map((item, index) => (
          <p key={index} style={labelStyle}>{item}
            <LinearProgressWithLabel value={ limitValue(data["skills"][item]) }/>
          </p>
        ))

      }

      <h2>Other Skills</h2>
      <p style={skillListStyle}>{ data["otherskills"].join(", ") }</p>
    </div>
  );


};


const LinearProgressWithLabel = (props) => {
  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>

      <Box sx={barBoxStyle}>
        <LinearProgress color="primary" variant="determinate" {...props} />
      </Box>

      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="lightblue">{`${Math.round(
          props.value,
        )}%`}</Typography>
      </Box>

    </Box>
  );

};


export default Skills
