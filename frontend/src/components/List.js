import React from 'react'

const style = {
  textAlign: 'left',
  marginLeft: '30%',
  marginRight: '10%'
};

const style2 = {
  /*
  textAlign: 'left',
  marginLeft: '40%',
  marginRight: '20%'
  */
  textAlign: 'left',
  display: 'table',
  marginLeft: '30%',
};

const List = ({title, data}) => {

  return (
    <>
      <h2>{title}</h2>
      <ul style={style}>
        {
          data[title.toLowerCase()].map((item, index) => (
            <li key={index}>{item}</li>
          ))

        }
      </ul>
    </>

  );



};


// TODO: live demo link
const LinkList = ({title, data}) => {

  return (
    <>
      <h2>{title}</h2>
      <ul style={style2} >
        {
          data[title.toLowerCase()].map((item, index) => (
            <li key={index} style={{marginBottom: '100px'}}>
              <a key={index} href={item["link"]}>{item["name"]}</a>
              <p>{item["description"]}</p>
            </li>
          ))

        }
      </ul>

    </>

  );

};



const exports = { List, LinkList }

export default exports;
