from flask import Flask
from flask import jsonify
from flask import request
from flask import make_response
from flask.logging import default_handler
from flask_cors import CORS
from flask_cors import cross_origin

from datetime import datetime
from datetime import timedelta

import logging
import logging.config
import json
import itertools
import sys
import os
import os.path
import toml
import utility


projectDir = os.getcwd()

log_config = toml.load(f"{projectDir}/log.toml", _dict=dict)["logger"]


#app = Flask(__name__, static_folder="/frontend/build", static_url_path="/")
app = Flask(__name__)
cors = CORS(app)
app.config["DEBUG"] = False
app.config["CORS_HEADERS"] = "Content-Type"

run_with_gunicorn = True if "gunicorn" in sys.argv[0] else False

utility.config_logging(log_config, projectDir, run_with_gunicorn, False)
log = logging.getLogger("api")
log.info(f"using gunicorn: {run_with_gunicorn}")


@app.route("/")
def index():
    return "<h1>nothing to see<h1>"

