from setuptools import find_packages, setup

module_name = "server"

setup(
    name=module_name,
    version="0.1",
    description="Server for Interactive CV",
    author="Severi Kujala",
    packages=find_packages(where="src"),
    package_dir={"": "src"},

    entry_points={
    },

    install_requires=[
        "Flask",
        "Flask-Cors",
        "gunicorn",
        "toml",
    ],

    extras_require={
        "test": [
            "wheel",
            "pytest",
            "requests",
        ],
    },

)

