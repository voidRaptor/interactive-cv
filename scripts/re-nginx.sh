#!/bin/sh
docker-compose down
docker volume rm iacv_static_dir -f
docker-compose build nginx --no-cache
docker-compose up nginx -d
